// SPDX-License-Identifier: GPL-2.1-or-later

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <stdlib.h>
#include <unistd.h>

#define WARNING(...) do { fprintf(stderr, __VA_ARGS__); fputc('\n', stderr); } while (0)
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define SERIAL_DEV "/dev/ttyACM0"

static int serial_open(void)
{
	struct termios t = {};
	int fd;

	fd = open(SERIAL_DEV, O_RDWR);
	if (fd < 0) {
		WARNING("Failed to open '%s': %s", SERIAL_DEV, strerror(errno));
		return -1;
	}

	if (tcgetattr(fd, &t)) {
		WARNING("tcgetattr: %s", strerror(errno));
		goto err;
	}

	cfmakeraw(&t);

	t.c_lflag &= ~ECHO;
	t.c_iflag |= ICRNL;
	t.c_cflag |= CLOCAL;

	t.c_cc[VMIN] = 0;
	t.c_cc[VTIME] = 10;

	cfsetispeed(&t, B9600);
	cfsetospeed(&t, B9600);

	if (tcsetattr(fd, TCSANOW, &t)) {
		WARNING("tcsetattr: %s", strerror(errno));
		goto err;
	}

	tcflush(fd, TCIFLUSH);

	return fd;
err:
	close(fd);
	return -1;
}

enum units {
	UNIT_NONE,
	UNIT_KELVIN,
	UNIT_CELSIUS,
	UNIT_VOLT_10BIT,
	UNIT_MILI_VOLT,
};

static const char *unit_name(enum units unit)
{
	switch (unit) {
	case UNIT_KELVIN:
		return "K";
	case UNIT_CELSIUS:
		return "C";
	case UNIT_NONE:
		return "";
	case UNIT_MILI_VOLT:
		return "mV";
	default:
		return "?";
	}
}

struct params {
	const char id;
	const char *opt;
	const char *desc;
	enum units unit;
	int setable;
};

#define MAX_PARAMS_V2 5

static const struct params params[] = {
	{'V', "--version", "Mutant hardware revision", UNIT_NONE, 0},
	{'T', "--it", "Internal temperature", UNIT_KELVIN, 0},
	{'P', "--tpt", "Temperature poweroff threshold", UNIT_KELVIN, 1},
	{'L', "--ld", "Led display", UNIT_NONE, 1},
	{'F', "--fw", "Firmware version", UNIT_NONE, 0},
	/* v3 only */
	{'B', "--bv", "Battery voltage", UNIT_VOLT_10BIT, 0},
	{'Q', "--bvt", "Battery voltage poweroff threshold", UNIT_VOLT_10BIT, 1},
	{'M', "--lb", "Led body", UNIT_NONE, 1},
	{'Z', "--buz", "Turn on buzzer for x ms", UNIT_NONE, 1},

	{'l', "--lcd", "Tuns power on/off for LCD", UNIT_NONE, 1},
	{'e', "--exp", "Tuns power on/off expansion board", UNIT_NONE, 1},
	{'s', "--sen", "Tuns power on/off for sensors", UNIT_NONE, 1},
};

/*
 * Unit mapping for program output and cmdline params.
 */
static enum units default_unit(enum units unit)
{
	switch (unit) {
	case UNIT_KELVIN:
		return UNIT_CELSIUS;
	case UNIT_VOLT_10BIT:
		return UNIT_MILI_VOLT;
	default:
		return unit;
	}
}

/*
 * Unit conversion for output and cmdline params.
 */
static int conv_unit(int val, enum units in, enum units out)
{
	if (in == out)
		return val;

	if (in == UNIT_KELVIN && out == UNIT_CELSIUS)
		return val - 273;

	if (in == UNIT_CELSIUS && out == UNIT_KELVIN)
		return val + 273;

	if (in == UNIT_VOLT_10BIT && out == UNIT_MILI_VOLT)
		return 5000 * val / 1024;

	if (in == UNIT_MILI_VOLT && out == UNIT_VOLT_10BIT)
		return 1024 * val / 5000;

	WARNING("Missing unit conversion %s -> %s\n",
	        unit_name(in), unit_name(out));

	return -1;
}

static int read_buf(int fd, char *buf, size_t buf_len)
{
	size_t pos = 0;

	while (pos < buf_len) {
		int ret = read(fd, buf+pos, buf_len-pos);

		if (ret < 0)
			return 0;

		pos += ret;

		if (buf[pos - 1] == '\n') {
			buf[pos - 1] = '\0';
			return 1;
		}
	}

	return 0;
}

static int get_val(int fd, const struct params *param, enum units out)
{
	char buf[3] = {'?', param->id, '\n'};
	char res[64];

	if (write(fd, buf, 3) != 3)
		WARNING("Failed to write request!");

	read_buf(fd, res, sizeof(res));

	return conv_unit(atoi(res+1), param->unit, out);
}

static void set_val(int fd, const struct params *param, int val)
{
	char buf[64];
	int len;

	val = conv_unit(val, default_unit(param->unit), param->unit);

	len = sprintf(buf, "!%c%i\n", param->id, val);

	if (write(fd, buf, len) != len)
		WARNING("Failed to write request!");
}

static void print_all(int fd)
{
	size_t i, max_params = ARRAY_SIZE(params);

	for (i = 0; i < max_params; i++) {
		enum units out = default_unit(params[i].unit);
		int val = get_val(fd, &params[i], out);

		if (i == 0) {
			switch (val) {
			case 3:
			break;
			case 2:
				max_params = MAX_PARAMS_V2;
			break;
			default:
				printf("Unknown hardware version %i\n", val);
			break;
			}
		}

		printf("%s: %i %s\n", params[i].desc, val, unit_name(out));
	}
}

static void print_help(void)
{
	size_t i;

	printf("Help\n----\n\n");

	for (i = 0; i < ARRAY_SIZE(params); i++) {
		if (i == MAX_PARAMS_V2)
			printf("------------ V3 only -------------\n\n");

		printf("%s in %s\n", params[i].desc,
		       unit_name(default_unit(params[i].unit)));

		printf(" query with %s\n", params[i].opt);
		if (params[i].setable)
			printf(" set with %s val\n", params[i].opt);

		printf("\n");
	}
}

static void print_val(int fd, const struct params *param)
{
	enum units out = default_unit(param->unit);

	printf("%i %s\n", get_val(fd, param, out), unit_name(out));
}

int main(int argc, char *argv[])
{
	int i, fd = 0;

	if (argc == 1) {
		fd = serial_open();
		if (fd < 0)
			return 1;

		print_all(fd);
		close(fd);
		return 0;
	}

	for (i = 1; i < argc; i++) {
		size_t j;

		if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
			print_help();
			return 0;
		}

		for (j = 0; j < ARRAY_SIZE(params); j++) {
			if (!strcmp(argv[i], params[j].opt)) {
				const char *val = NULL;

				if (argv[i+1] && argv[i+1][0] != '-')
					val = argv[++i];

				if (val && !params[j].setable) {
					printf("%s is not settable!\n", params[j].desc);
					goto exit;
				}

				if (!fd) {
					fd = serial_open();
					if (fd < 0)
						return 1;
				}

				if (!val)
					print_val(fd, &params[j]);
				else
					set_val(fd, &params[j], atoi(val));

				break;
			}
		}

		if (j == ARRAY_SIZE(params)) {
			printf("Invalid option %s\n", argv[i]);
			goto exit;
		}
	}

exit:
	if (fd)
		close(fd);
	return 0;
}
