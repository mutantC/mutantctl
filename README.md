# mutantctl

This is a command line tool that can talk to the arduino that implements
keyboard and power management on mutant.

![screenshot](mutantctl.png "mutantctl")

## Supported commands on V2 hardware revision

* Read hardware revision (reported as V2)
* Read internal temperature
* Read/Write temperature poweroff threshold

## Supported commands on V3 hardware revision

* TBD
